import ktkws

import grpc
import gigagenieRPC_pb2
import gigagenieRPC_pb2_grpc
import MicrophoneStream as MS
import user_auth as UA
import audioop
from ctypes import *

ERROR_HANDLER_FUNC = CFUNCTYPE(None, c_char_p, c_int, c_char_p, c_int, c_char_p)
def py_error_handler(filename, line, function, err, fmt):
    dummy_var = 0
c_error_handler = ERROR_HANDLER_FUNC(py_error_handler)
asound = cdll.LoadLibrary('libasound.so')
asound.snd_lib_error_set_handler(c_error_handler)

class Voice2Text(object):
    def __init__(self, sample_rate, chunk, host='gate.gigagenie.ai', port=4080):
        self.host = host
        self.port = port
        self.sample_rate = sample_rate
        self.chunk = chunk

        self.resultText = ''

    def generate_request(self):
        with MS.MicrophoneStream(self.sample_rate, self.chunk) as stream:
            audio_generator = stream.generator()

            for content in audio_generator:
                message = gigagenieRPC_pb2.reqVoice()
                message.audioContent = content
                yield message

                rms = audioop.rms(content, 2)
                # print_rms(rms)

    def getVoice2Text(self):
        channel = grpc.secure_channel('{}:{}'.format(self.host, self.port), UA.getCredentials())
        stub = gigagenieRPC_pb2_grpc.GigagenieStub(channel)
        request = self.generate_request()
        resultText = ''
        for response in stub.getVoice2Text(request):
            if response.resultCd == 200:  # partial
                print('resultCd=%d | recognizedText= %s'% (response.resultCd, response.recognizedText))
                resultText = response.recognizedText
            elif response.resultCd == 201:  # final
                print('resultCd=%d | recognizedText= %s'% (response.resultCd, response.recognizedText))
                resultText = response.recognizedText
                break
            else:
                print('resultCd=%d | recognizedText= %s'% (response.resultCd, response.recognizedText))
                break
        self.resultText = resultText