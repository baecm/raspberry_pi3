import grpc
import gigagenieRPC_pb2
import gigagenieRPC_pb2_grpc
import MicrophoneStream as MS
import user_auth as UA
from ctypes import *

import wave
import pyaudio

import argparse

# ERROR_HANDLER_FUNC = CFUNCTYPE(None, c_char_p, c_int, c_char_p, c_int, c_char_p)
# def py_error_handler(filename, line, function, err, fmt):
#   dummy_var = 0
# c_error_handler = ERROR_HANDLER_FUNC(py_error_handler)
# asound = cdll.LoadLibrary('libasound.so')
# asound.snd_lib_error_set_handler(c_error_handler)

HOST = 'gate.gigagenie.ai'
PORT = 4080

SAMPLE_RATE = 16000
CHUNK = 512

def play(filename):
    wf = wave.open(filename, 'rb')
    pa = pyaudio.PyAudio()
    stream = pa.open(
        format=pa.get_format_from_width(wf.getsampwidth()),
        rate=wf.getframerate(),
        channels=wf.getnchannels(),
        output=True
    )
    data = wf.readframes(CHUNK)
    while data != b'':
        stream.write(data)
        data = wf.readframes(CHUNK)

    pa.terminate()
    stream.close()


def getText2VoiceStream(inText, inFileName):
    channel = grpc.secure_channel('{}:{}'.format(HOST, PORT), UA.getCredentials())
    stub = gigagenieRPC_pb2_grpc.GigagenieStub(channel)

    message = gigagenieRPC_pb2.reqText()
    message.lang = 0
    message.mode = 0
    message.text = inText
    writeFile = open(inFileName, 'wb')
    for response in stub.getText2VoiceStream(message):
        if response.HasField("resOptions"):
            print("\n\nResVoiceResult: %d" % (response.resOptions.resultCd))
        if response.HasField("audioContent"):
            print("Audio Stream\n\n")
            writeFile.write(response.audioContent)
    writeFile.close()
    return response.resOptions.resultCd

def main(args):
    getText2VoiceStream("안녕하세요. 반갑습니다.", args.file)
    # MS.play_file(args.file)
    play(args.file)
    print(args.file + "이 생성되었으니 파일을 확인바랍니다. \n")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--text', '-t',
        type=str,
        default='안녕하세요. 이것은 TTS 테스트 결과입니다.',
        help='TTS 테스트 문자열을 입력하세요.')
    parser.add_argument(
        '--file', '-f',
        type=str,
        default='result_tts.wav',
        help='출력 파일명 (default: result_tts.wav)')
    args = parser.parse_args()
    main(args)